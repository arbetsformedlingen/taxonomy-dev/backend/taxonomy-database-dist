(ns jobtechdev.taxonomy-database-dist
  (:require [clojure.java.io :as io]
            [taoensso.timbre :as log])
  (:gen-class))

(def ^:export available-taxonomies
  "The available taxonomies in this library."
  {:v21 "taxonomy-v21.nippy"
   :v20 "taxonomy-v20.nippy"
   :v2001-AF "taxonomy-AF-v2001.nippy"})

(defn ^:export open-taxonomy
  "Returns an opened taxonomy resource.
   `(open-taxonomy)` yields the most recent version and
   `(open-taxonomy v)` opens the taxonomy with the corresponding
   key `v` in the map `available-taxonomies`.
   If there is no corresponding version to `v` `open-taxonomy` fails with `nil`."
  ([]
   (let [default-version :v21]
     (log/debug "Using default version" default-version)
     (open-taxonomy default-version)))
  ([taxonomy-version]
   (if (contains? available-taxonomies taxonomy-version)
     (do
       (log/debug "Opening taxonomy version" taxonomy-version)
       (io/resource (taxonomy-version available-taxonomies)))
     (do
       (log/error "Invalid taxonomy version" taxonomy-version)
       nil))))