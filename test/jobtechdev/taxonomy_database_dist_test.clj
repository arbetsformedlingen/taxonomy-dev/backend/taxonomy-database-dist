(ns jobtechdev.taxonomy-database-dist-test
  (:require [clojure.java.io :as io]
            [clojure.test :refer [deftest is testing]]
            [jobtechdev.taxonomy-database-dist :as tdd]
            [taoensso.timbre :as timbre]))

(defmacro capture-log
  [config & body]
  `(let [output# (atom nil)]
     (timbre/with-config
       (assoc ~config
              :appenders ; Overwrite appenders
              {:capture
               {:enabled? true
                :fn (fn [data#] (swap! output# #(conj (or % []) data#)))}})
       ~@body)
     @output#))

(deftest available-taxonomies-test
  (testing "That there are taxonomies available"
    (is (= 3 (count tdd/available-taxonomies)))))

(deftest open-taxonomy-test
  (testing "We can open the default taxonomy"
    (let [result (atom nil)
          log-data (capture-log
                    {:level :debug}
                    (reset! result (tdd/open-taxonomy)))]
      (is (= [[:debug ["Using default version" :v21]]
              [:debug ["Opening taxonomy version" :v21]]]
             (map (juxt :level :vargs) log-data)))
      (is (io/file @result))))
  (testing "We can open v20"
    (let [result (atom nil)
          log-data (capture-log
                    {:level :debug}
                    (reset! result (tdd/open-taxonomy :v20)))]
      (is (= [[:debug ["Opening taxonomy version" :v20]]]
             (map (juxt :level :vargs) log-data)))
      (is (io/file @result))))
  (testing "We can open the 2001 AF taxonomy"
    (let [result (atom nil)
          log-data (capture-log
                    {:level :debug}
                    (reset! result (tdd/open-taxonomy :v2001-AF)))]
      (is (= [[:debug ["Opening taxonomy version" :v2001-AF]]]
             (map (juxt :level :vargs) log-data)))
      (is (io/file @result))))
  (testing "We return `nil` for invalid version"
    (let [result (atom nil)
          log-data (capture-log
                    {:level :debug}
                    (reset! result (tdd/open-taxonomy :not-a-version)))]
      (is (= [[:error ["Invalid taxonomy version" :not-a-version]]]
             (map (juxt :level :vargs) log-data)))
      (is (nil? @result)))))
