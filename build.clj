(ns build
  (:refer-clojure :exclude [test])
  (:require [clojure.tools.build.api :as b]))

(def lib-name 'se.jobtechdev/taxonomy-database-dist)
(def version "21.0.1")

(def basis (b/create-basis {:project "deps.edn"}))

(def build-folder "target")
(def class-dir (str build-folder "/classes"))

(def jar-file-name (format "%s/%s-%s.jar" build-folder (name lib-name) version))

(def copy-src ["src" "resources"])

(defn clean [_]
  (b/delete {:path "target"}))

(defn ^:export jar [_]
  (clean nil)
  (b/compile-clj {:basis basis
                  :src-dirs ["src"]
                  :class-dir class-dir})
  (b/copy-dir {:src-dirs copy-src
               :target-dir class-dir})
  (b/write-pom {:basis basis
                :lib lib-name
                :version version
                :scm
                {:url "https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/taxonomy-database-dist"
                 :connection "scm:git:git://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/taxonomy-database-dist.git"
                 :developerConnection "scm:git:ssh://git@gitlab.com/arbetsformedlingen/taxonomy-dev/backend/taxonomy-database-dist.git"
                 :tag version}
                :src-dirs ["src"]
                :class-dir class-dir})
  (b/jar {:class-dir class-dir
           :jar-file jar-file-name}))
